@extends('layout.main')

@section('title','register')

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up form</h3>

<!-- Awal Form -->
<form action="/welcome" method="post">
    @csrf
    <label for="fname">First name:</label><br>
    <input type="text" id="fname" name="fname"><br>
    <label for="lname">Last name:</label><br>
    <input type="text" id="lname" name="lname"><br>
    <label for="gender">Gender</label><br>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label><br>
    <label for="nationality">Nationality:</label><br>
    <select id="nationality" name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="singapura">Singapura</option>
        <option value="vietnam">Vietnam</option>
    </select>
    <p>Language Spoken :</p>
    <input type="checkbox" name="indo" id="indo" value="Bahasa Indonesia">
    <label for="indo">Bahasa Indonesia</label><br>
    <input type="checkbox" name="english" id="english" value="English">
    <label for="indo">English</label><br>
    <input type="checkbox" name="other" id="other" value="Other">
    <label for="indo">Other</label><br>
    <p>Bio</p>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
    <button type="submit">Sign Up</button>
</form>
<!-- Akhir Form -->
@endsection